# tarefa 3.1

UAB - Programação Web Avançada Tarefa 3.1

## Descrição
```
Tarefa 3.1: Wireframe, Protótipo e Mockup: Diferenças? Que contributos da semântica do HTML5 dá à prototipagem?

b) Cada aluno deve planear a inclusão das duas novas entidades sponsors e experts na plataforma através da realização dos conceitos de prototipagem: wireframes, mockups e protótipos. A informação relativa a cada uma das entidades faz parte do planeamento a ser realizado, ficando a decisão da informação necessária e suficiente no aluno.
Os ficheiros produzidos devem ser colocados na área individual do Git num projeto chamado tarefa 3.1.
A aplicação dos conceitos wireframe e mockup deve ter uma representação para browser e para mobile (para o desenho dos conceitos pode utilizar a aplicação web disponível em draw.io).
O ficheiro protótipo de cada nova entidade já deve ser incluído na Página de Administração utilizando uma estrutura de documento baseada em HTML5. O funcionamento da componente de Back-office deve ser testado, com a navegação e visualização das novas entidades devidamente integradas nos vários blocos da aplicação web.
```

# Wireframes

Os wireframes presentes na pasta Wireframes, foi desenvolvido recorrendo à aplicação Mockflow https://www.mockflow.com/

# Mockups

Os mockups presentes na pasta Mockups, foi desenvolvido recorrendo à aplicação FIGMA https://www.figma.com/

# Protótipo

O Protótipo foi desenvolvido recorrendo ao HTML5 e baseado na livraria Bootstrap, e nos estilos utilizados na aplicação ANIMALEC disponibilizado no livro recomendado.<br>
O Protótipo pode ser acedido http://pwa-tarefa3-1.nsmeiw.pt


